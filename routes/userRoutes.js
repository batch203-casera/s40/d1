const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

console.log(userControllers);

// Routes for checking email
router.post("/checkEmail", userControllers.checkEmailExists);

// Route for user registration
router.post("/register", userControllers.registerUser);

// Route for user authentication
router.post("/login", userControllers.loginUser);

// Route for user details
router.get("/details", auth.verify, userControllers.getProfile);

module.exports = router;